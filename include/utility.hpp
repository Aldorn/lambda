#include <optional>
#include <variant>

namespace lambda {

// helper type for the visitor #4
template<class... Ts> struct overloaded : Ts... {
    using Ts::operator()...;
};
// explicit deduction guide (not needed as of C++20)
template<class... Ts> overloaded(Ts...) -> overloaded<Ts...>;

template<typename T, typename U>
std::optional<T> or_else(const std::optional<T>& first, U&& second) {
    if (first) return first;
    else return second();
}

template<typename T, typename U>
std::optional<T> and_then(const std::optional<T>& first, U&& second) {
    if (!first) return first;
    else return second(first);
}

template<typename T, typename U>
std::optional<T> operator &(const std::optional<T>& first, U&& second) {
    return or_else(first, std::forward<U>(second));
}

template<typename T, typename Variant>
std::optional<T> maybe_get(Variant&& variant) {
    if (std::holds_alternative<T>(variant))
        return std::get<T>(variant);
    else return {};
}

}
