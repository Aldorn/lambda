#ifndef LAMBDAPARSER_HPP
#define LAMBDAPARSER_HPP

#include <iosfwd>

#include "lambdaterm.hpp"

namespace lambda::parser {
class ParseException : public std::exception {
private:
    size_t m_line;
    size_t m_pos;
    size_t m_index;
    std::string m_msg;
public:
    ParseException();
    ParseException(const std::string& error, size_t line, size_t pos);
    inline virtual const char* what() const noexcept {return m_msg.c_str(); };
    inline size_t line() const { return m_line; }
    inline size_t pos() const { return m_pos; }
};

LambdaTerm parseTerm(const std::string& in);
LambdaTerm parseTerm(std::istream& in);
} // namespace lambda

#endif // LAMBDAPARSER_HPP
