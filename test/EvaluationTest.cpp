#define BOOST_TEST_MODULE LambdaEvaluation
#include <boost/test/unit_test.hpp>

#include "lambdaparser.hpp"
#include "evaluators.hpp"

namespace lambda {
std::ostream& operator<<(std::ostream& cout, const lambda::LambdaTerm& term) {
    return cout << to_string(term);
}
}
using namespace lambda;

BOOST_AUTO_TEST_SUITE(execution_orders)

BOOST_AUTO_TEST_CASE(basic_redex_no) {
    auto term = parser::parseTerm("(\\x.x) y");
    BOOST_TEST_CHECKPOINT("normal order");
    auto redex = normalOrderRedex(term);
    BOOST_TEST_REQUIRE(!!redex);
    BOOST_CHECK_PREDICATE(isRedex, (redex.value().get()));
    BOOST_TEST(redex.value().get() == term);
}

BOOST_AUTO_TEST_CASE(basic_redex_ao) {
    auto term = parser::parseTerm("(\\x.x) y");
    BOOST_TEST_CHECKPOINT("applicative order");
    auto redex = applicativeOrderRedex(term);
    BOOST_TEST_REQUIRE(!!redex);
    BOOST_CHECK_PREDICATE(isRedex, (redex.value().get()));
    BOOST_TEST(redex.value().get() == term);
}

BOOST_AUTO_TEST_CASE(basic_redex_cbn) {
    auto term = parser::parseTerm("(\\x.x) y");
    BOOST_TEST_CHECKPOINT("call by name");
    auto redex = callByNameRedex(term);
    BOOST_TEST_REQUIRE(!!redex);
    BOOST_CHECK_PREDICATE(isRedex, (redex.value().get()));
    BOOST_TEST(redex.value().get() == term);
}

BOOST_AUTO_TEST_CASE(basic_redex_cbv) {
    {
        auto term = parser::parseTerm("(\\x.x) y");
        BOOST_TEST_CHECKPOINT("call by value");
        auto redex = callByValueRedex(term);
        BOOST_TEST_REQUIRE(!redex);
    }
    {
        auto term = parser::parseTerm("(\\x.x) (\\y.y)");
        BOOST_TEST_CHECKPOINT("call by value");
        auto redex = callByValueRedex(term);
        BOOST_TEST_REQUIRE(!!redex);
        BOOST_CHECK_PREDICATE(isRedex, (redex.value().get()));
        BOOST_TEST(redex.value().get() == term);
    }
}

BOOST_AUTO_TEST_CASE(no_redex) {
    auto term = parser::parseTerm("x y");
    {
        auto redex = normalOrderRedex(term);
        BOOST_TEST(!redex);
    }
    {
        auto redex = applicativeOrderRedex(term);
        BOOST_TEST(!redex);
    }
    {
        auto redex = callByNameRedex(term);
        BOOST_TEST(!redex);
    }
    {
        auto redex = callByValueRedex(term);
        BOOST_TEST(!redex);
    }
}

BOOST_AUTO_TEST_CASE(bound_redex) {
    auto term = parser::parseTerm("\\x.(\\x.x) x");
    auto redex = normalOrderRedex(term);
    LambdaTerm expectedRedex = Application(Abstraction("x", 0), 0);
    BOOST_TEST(redex.value().get() == expectedRedex);
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(reduction)

BOOST_AUTO_TEST_CASE(basic_redex) {
    auto term = parser::parseTerm("(\\x.x) x");
    auto& redex = normalOrderRedex(term).value().get();
    BOOST_TEST(term == redex);
    reduceRedex(redex);
    BOOST_TEST(term == parser::parseTerm("x"));
}

BOOST_AUTO_TEST_CASE(renaming) {
    auto term = parser::parseTerm("\\z.(\\x.\\y.x) (\\y.z)");
    auto& redex = applicativeOrderRedex(term).value().get();
    BOOST_TEST(redex == LambdaTerm(Application(Abstraction("x", Abstraction("y", 1)), Abstraction("y", 1))));
    reduceRedex(redex);
    BOOST_TEST(term == parser::parseTerm("\\z.\\y.\\y.z"));
}

BOOST_AUTO_TEST_CASE(name_collision) {
    auto term = parser::parseTerm("\\x.(\\y.\\x.x y) x");
    auto& redex = applicativeOrderRedex(term).value().get();
    reduceRedex(redex);
    BOOST_CHECK_PREDICATE(equivalent, (term)(parser::parseTerm("\\x.\\x1.x1 x")));
}

BOOST_AUTO_TEST_CASE(long_reduction) {
    //auto term = parser::parseTerm(R"((\n.(\p.p \a.\b.a) (n (\p.(\f.f (p \a.\b.b) (\s.\z. (p \a.\b.a) s ((p \a.\b.b) s z) ))) (\f.f (\s.\z.z) (\s.\z.s z)) )) (\s.\z.s (s z)))");
    auto term = parser::parseTerm(R"((\m. \n. \s. \z. m (n s) z) (\s. \z. s (s z)) (\s. \z. s (s (s z))))");
    auto redex = applicativeOrderRedex(term);
    size_t steps = 10;
    while (redex && steps > 0) {
        reduceRedex(redex.value().get());
        BOOST_TEST_INFO(term);
        BOOST_TEST_INFO(representation(redex.value()));
        redex = applicativeOrderRedex(term);
        --steps;
    }
    BOOST_TEST(steps > 0, "reduction took too many steps");
    BOOST_TEST(term == parser::parseTerm("\\s.\\z.s (s (s (s (s (s z)))))"));
}

BOOST_AUTO_TEST_CASE(long_fibo_reduction) {
    auto term = parser::parseTerm(R"((\n.(\p.p \a.\b.a) (n (\p.(\f.f (p \a.\b.b) (\s.\z. (p \a.\b.a) s ((p \a.\b.b) s z) ))) (\f.f (\s.\z.z) (\s.\z.s z)) )) (\s.\z.s (s (s (s (s z))))))");
    auto redex = normalOrderRedex(term);
    size_t steps = 1500;
    while (redex && steps > 0) {
        reduceRedex(redex.value().get());
        BOOST_TEST_INFO(term);
        BOOST_TEST_INFO(representation(redex.value()));
        redex = normalOrderRedex(term);
        --steps;
    }
    BOOST_TEST(steps > 0, "reduction took too many steps");
    BOOST_TEST(term == parser::parseTerm("\\s.\\z.s (s (s (s (s z))))"));
}

BOOST_AUTO_TEST_SUITE_END()
