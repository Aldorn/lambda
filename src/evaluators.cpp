#include "evaluators.hpp"

#include <vector>
#include <map>
#include <cassert>

#include "utility.hpp"

namespace lambda {

namespace {
LambdaTerm recalculatedRank(const LambdaTerm& term, size_t minRank, size_t downDiff) {
    auto r = visit(overloaded {
        [minRank, downDiff](const Application& app) -> LambdaTerm {
            return Application(
                recalculatedRank(app.function(), minRank, downDiff),
                recalculatedRank(app.argument(), minRank, downDiff)
            );
        },
        [minRank, downDiff](const Abstraction& abs) -> LambdaTerm {
            return Abstraction(
                abs.name(),
                recalculatedRank(abs.term(), minRank + 1, downDiff)
            );
        },
        [minRank, downDiff](const Variable& var) -> LambdaTerm {
            if (var.rank() >= minRank) {
                return Variable(var.rank() + downDiff);
            } else {
                return var;
            }
        },
        [](const auto& x) -> LambdaTerm { return x; },
    }, term);
    return r;
}

LambdaTerm& substituteVariable(
        size_t rank,
        LambdaTerm& inTerm,
        const LambdaTerm& withTerm)
{
    visit(overloaded {
        [rank, &withTerm](Application& app) {
            substituteVariable(rank, app.function(), withTerm);
            substituteVariable(rank, app.argument(), withTerm);
        },
        [rank, &withTerm](Abstraction& abs) {
            substituteVariable(rank + 1, abs.term(), withTerm);
        },
        [rank, &withTerm, &inTerm](Variable& var) {
            if (var.rank() == rank) inTerm = recalculatedRank(withTerm, 0, rank);
            else if (var.rank() > rank) --var.rank();
        },
        [](const auto&) { }
    }, inTerm);
    return inTerm;
}

} // namespace

bool isAbstraction(const LambdaTerm& term) {
    return visit(overloaded {
        [](const auto&) -> bool { return false; },
        [](const Abstraction&) -> bool { return true; }
    }, term);
}

bool isRedex(const LambdaTerm& term) {
    return visit(overloaded {
        [](const auto&) -> bool { return false; },
        [](const Application& app) -> bool { return isAbstraction(app.function()); }
    }, term);
}

OptionalTerm callByNameRedex(LambdaTerm &term) {
    return visit(overloaded {
        [](const auto&) -> OptionalTerm { return {};},
        [&term](Application& app) -> OptionalTerm {
            if (isAbstraction(app.function())) return term;
            else return callByNameRedex(app.function()) & [&app](){return callByNameRedex(app.argument());};
        }
    }, term);
}

OptionalTerm callByValueRedex(LambdaTerm &term) {
    return visit(overloaded {
        [](const auto&) -> OptionalTerm { return {};},
        [&term](Application& app) -> OptionalTerm {
            if (isAbstraction(app.function()) && isAbstraction(app.argument())) return term;
            else return callByValueRedex(app.function()) & [&app](){return callByValueRedex(app.argument());};
        }
    }, term);
}

OptionalTerm normalOrderRedex(LambdaTerm &term) {
    return visit(overloaded {
        [](const auto&) -> OptionalTerm { return {};},
        [](Abstraction& abs) -> OptionalTerm {
            return normalOrderRedex(abs.term());
        },
        [&term](Application& app) -> OptionalTerm {
            if (isAbstraction(app.function())) return term;
            else return normalOrderRedex(app.function()) & [&app](){return normalOrderRedex(app.argument());};
        }
    }, term);
}

OptionalTerm applicativeOrderRedex(LambdaTerm &term) {
    return visit(overloaded {
        [](const auto&) -> OptionalTerm { return {};},
        [](Abstraction& abs) -> OptionalTerm {
            return applicativeOrderRedex(abs.term());
        },
        [&term](Application& app) -> OptionalTerm {
            if (isAbstraction(app.function())) return term;
            else {
                return applicativeOrderRedex(app.argument())
                    & [&app]{ return applicativeOrderRedex(app.function()); }
                             & []() -> OptionalTerm { return {}; };
            }
        }
    }, term);
}

void reduceRedex(LambdaTerm &redex) {
    visit(overloaded{
        [](const auto&) { },
        [&redex](Application& app) mutable { visit(overloaded{
            [](const auto&) { },
            [&redex, &app] (Abstraction& abs) mutable {
                redex.swap(substituteVariable(0, abs.term(), app.argument()));
            }
        }, app.function());}
    }, redex);
}

} // namespace lambda
