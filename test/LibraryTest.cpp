#define BOOST_TEST_MODULE LambdaParsing
#include <boost/test/unit_test.hpp>

#include "lambdaparser.hpp"

namespace lambda {
std::ostream& operator<<(std::ostream& cout, const lambda::LambdaTerm& term) {
    return cout << representation(term);
}
}

using namespace lambda;
using namespace std::literals;

BOOST_AUTO_TEST_SUITE(parser_suite);

BOOST_AUTO_TEST_CASE(parse_simple) {
    auto term = lambda::parser::parseTerm("\\x.x");
    LambdaTerm expectedTerm = Abstraction("x"s, Variable(0));
    BOOST_TEST(term == expectedTerm);
    LambdaTerm secondExpected = Abstraction("x"s, Abstraction("y"s, Application(Variable(1), Variable(0))));
    BOOST_TEST(lambda::parser::parseTerm("\\x.\\y.x y") == secondExpected);
}

BOOST_AUTO_TEST_CASE(parse_free) {
    auto term = lambda::parser::parseTerm("x");
    LambdaTerm expected = FreeVariable("x");
    BOOST_TEST(term == expected);
}

BOOST_AUTO_TEST_CASE(all_nodes) {
    auto term = lambda::parser::parseTerm("x \\x.\\y.x a (\\a.a) y");
    LambdaTerm inner = Application(Application(Application(1, "a"s), Abstraction("a"s, Variable(0))), Variable(0));
    LambdaTerm expected = Application(FreeVariable("x"), Abstraction("x"s, Abstraction("y", inner)));
    BOOST_TEST(term == expected);
}

BOOST_AUTO_TEST_CASE(var_name) {

    auto term = lambda::parser::parseTerm("x \\x.x");
    LambdaTerm expected = Application("x"s, Abstraction("x", Variable(0)));
    BOOST_TEST(term == expected);
    auto app = std::get<Application>(term);
    auto freeVar = std::get<FreeVariable>(app.function());
    BOOST_TEST(freeVar.name() == "x"s);
    auto abs = std::get<Abstraction>(app.argument());
    BOOST_TEST(abs.name() == "x"s);
}

BOOST_AUTO_TEST_CASE(associativity) {
    {
        auto term = parser::parseTerm("a b c d");
        auto expected = parser::parseTerm("((a b) c) d");
        BOOST_TEST(term == expected);
    }
    {
        auto term = parser::parseTerm("x y \\x.x y");
        auto expected = parser::parseTerm("(x y) (\\x.(x y))");
        BOOST_TEST(term == expected);
    }
}

BOOST_AUTO_TEST_SUITE_END()

BOOST_AUTO_TEST_SUITE(basic_functionality)

BOOST_AUTO_TEST_CASE(to_string, *boost::unit_test::disabled()) {
    auto code = R"(x (\x1.\y.x1 x1 y) )";
    BOOST_TEST(lambda::to_string(lambda::parser::parseTerm(code)) == code);
}

BOOST_AUTO_TEST_CASE(re_parse) {
    auto code1 = R"(\x1.\x.\x.\x.\x.\x.\x.\x.\x.\x.\x.\x.\x.x1)";
    for (const auto& c : {code1}) {
        auto parsed = parser::parseTerm(c);
        BOOST_TEST_INFO(lambda::to_string(parsed));
        BOOST_CHECK_PREDICATE(lambda::equivalent, (parser::parseTerm(lambda::to_string(parsed)))(parsed));
    }
}

BOOST_AUTO_TEST_SUITE_END()
