#ifndef EVALUATORS_HPP
#define EVALUATORS_HPP

#include <lambdaterm.hpp>
#include <optional>

namespace lambda {
bool is_value(const LambdaTerm& t);

bool isRedex(const LambdaTerm& term);
bool isAbstraction(const LambdaTerm& term);

using OptionalTerm = std::optional<std::reference_wrapper<LambdaTerm>>;
OptionalTerm callByNameRedex(LambdaTerm& term);
OptionalTerm callByValueRedex(LambdaTerm& term);
OptionalTerm normalOrderRedex(LambdaTerm& term);
OptionalTerm applicativeOrderRedex(LambdaTerm& term);

void reduceRedex(LambdaTerm& redex);

} // namespace lambda


#endif // EVALUATORS_HPP
