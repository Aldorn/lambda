cmake_minimum_required(VERSION 2.8)
set(CMAKE_CXX_STANDARD 17)

project(LibLambda)
add_subdirectory(src)
add_subdirectory(test)

# add_library(${PROJECT_NAME} "src/lambdaterm.cpp")
#add_executable(${PROJECT_NAME} "main.cpp")
