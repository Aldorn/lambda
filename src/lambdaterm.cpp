#include "lambdaterm.hpp"

#include <string>
#include <locale>
#include <vector>
#include <set>
#include <map>

#include "utility.hpp"

using namespace std::literals;

namespace lambda {

Variable::Variable(size_t depth)
    : m_rank{depth}
{ }

bool Variable::operator==(const Variable &other) const {
    return m_rank == other.rank();
}

bool Variable::operator!=(const Variable &other) const {
    return !(other == *this);
}

//Application::Application(std::unique_ptr<LambdaTerm> &&function, std::unique_ptr<LambdaTerm> &&argument)
//    : m_function(std::move(function)), m_argument(std::move(argument))
//{ }

Application::Application(LambdaTerm &&function, LambdaTerm &&argument)
    : m_function(std::make_unique<LambdaTerm>(std::move(function))),
      m_argument(std::make_unique<LambdaTerm>(std::move(argument)))
{ }

Application::Application(const LambdaTerm &function, const LambdaTerm &argument)
    : m_function(std::make_unique<LambdaTerm>(function)),
      m_argument(std::make_unique<LambdaTerm>(argument))
{ }

Application::Application(const Application& other)
    : m_function(std::make_unique<LambdaTerm>(other.function())),
      m_argument(std::make_unique<LambdaTerm>(other.argument()))
{ }

Application &Application::operator=(const Application &other) {
    m_function = std::make_unique<LambdaTerm>(other.function());
    m_argument = std::make_unique<LambdaTerm>(other.argument());
    return *this;
}

Application &Application::operator=(Application &&other) {
    std::swap(m_function, other.m_function);
    std::swap(m_argument, other.m_argument);
    return *this;
}

bool Application::operator==(const Application &other) const {
    return function() == other.function() && argument() == other.argument();
}

bool Application::operator!=(const Application &other) const {
    return !(*this == other);
}

const LambdaTerm &Application::function() const {
    return *m_function;
}

LambdaTerm &Application::function() {
    return *m_function;
}

const LambdaTerm &Application::argument() const {
    return *m_argument;
}

LambdaTerm &Application::argument() {
    return *m_argument;
}

// Abstraction::Abstraction(const std::string& name, std::unique_ptr<LambdaTerm>&& term)
//     : m_name(name), m_term(std::move(term))
// { }

Abstraction::Abstraction(const std::string& name, LambdaTerm &&term)
    : m_name(name), m_term(std::make_unique<LambdaTerm>(std::move(term)))
{ }

Abstraction::Abstraction(const std::string& name, const LambdaTerm &term)
    : m_name(name), m_term(std::make_unique<LambdaTerm>(term))
{ }

Abstraction::Abstraction(const Abstraction & other)
    : m_name(other.name()), m_term(std::make_unique<LambdaTerm>(other.term()))
{ }

Abstraction &Abstraction::operator=(const Abstraction &other) {
    m_term = std::make_unique<LambdaTerm>(other.term());
    return *this;
}

Abstraction &Abstraction::operator=(Abstraction &&other) {
    std::swap(m_term, other.m_term);
    return *this;
}

bool Abstraction::operator==(const Abstraction &other) const {
    return name() == other.name() && term() == other.term();
}

bool Abstraction::operator!=(const Abstraction &other) const {
    return !(*this == other);
}

const LambdaTerm &Abstraction::term() const {
    return *m_term;
}

LambdaTerm &Abstraction::term() {
    return *m_term;
}

const std::string &Abstraction::name() const {
    return m_name;
}

std::string &Abstraction::name() {
    return m_name;
}

/* precedence:
 * Abstraction
 * Application
 * Variable
 */
namespace {

std::string parenthesized(const std::string& s, bool p = true) {
    if (p)
        return "("s + s + ")"s;
    else
        return s;
}

constexpr const int APPLICATION_PRECEDENCE = 1;
constexpr const int ABSTRACTION_PRECEDENCE = 0;

namespace  {
struct FreeVars {
    std::set<std::string> vars;
    virtual std::set<std::string> operator()(const Application& app) {
        visit(*this, app.function());
        visit(*this, app.argument());
        return vars;
    }

    virtual std::set<std::string> operator()(const Abstraction& abs) {
        return visit(*this, abs.term());
    }

    virtual std::set<std::string> operator()(const Variable&) {
        return vars;
    }

    virtual std::set<std::string> operator()(const FreeVariable& fv) {
        vars.insert(fv.name());
        return vars;
    }
};
} // namespace

struct ToStringVisitor {
    std::vector<std::string> names;
    std::set<std::string> usedNames;
    ToStringVisitor(const LambdaTerm& term);

    std::string unusedName(const std::string& preferred);

    std::string operator()(const Application& app, int prec = 0);
    std::string operator()(const Abstraction& abs, int prec = 0);
    std::string operator()(const Variable& var, int prec = 0);
    std::string operator()(const FreeVariable& var, int prec = 0);
};

ToStringVisitor::ToStringVisitor(const LambdaTerm &term)
    : usedNames(visit(FreeVars{}, term))
{ }

std::string ToStringVisitor::unusedName(const std::string &preferred) {
    if (usedNames.count(preferred)) {
        for (size_t i = 1; ; ++i) {
            auto name = preferred + std::to_string(i);
            if (!usedNames.count(name)) return name;
        }
    } else {
        return preferred;
    }
}

std::string ToStringVisitor::operator()(const Application &app, int prec) {
    using iv = std::variant<int>;
    auto fString = visit(*this, app.function(), iv{APPLICATION_PRECEDENCE});
    auto argString = visit(*this, app.argument(), iv{APPLICATION_PRECEDENCE + 1});
    return parenthesized(fString + " "s + argString, prec > APPLICATION_PRECEDENCE);
}

std::string ToStringVisitor::operator()(const Abstraction &abs, int prec) {
    auto name = unusedName(abs.name());
    usedNames.insert(name);
    names.push_back(name);
    std::string varString = (*this)(0);
    std::string termString = visit(*this, abs.term(), std::variant<int>{ABSTRACTION_PRECEDENCE});
    names.pop_back();
    usedNames.erase(name);
    return parenthesized("\u03BB"s + varString + "." + termString, prec > ABSTRACTION_PRECEDENCE);
}

std::string ToStringVisitor::operator()(const Variable &var, int) {
    if (var.rank() >= names.size())
        return "#invalid#";
    return names[names.size() - var.rank() - 1];
}

std::string ToStringVisitor::operator()(const FreeVariable &var, int) {
    return var.name();
}

class RepresentationVisitor {
public:
    virtual std::string operator()(const Application& app) {
        auto left = visit(*this, app.function());
        auto right = visit(*this, app.argument());
        return "App("s + left + ", " + right + ")"s;
    }

    virtual std::string operator()(const Abstraction& abs) {
        auto termString = visit(*this, abs.term());
        return "Abs("s + abs.name() + ", "s + termString + ")"s;
    }

    virtual std::string operator()(const Variable& var) {
        return "Var("s + std::to_string(var.rank()) + ")"s;
    }

    virtual std::string operator()(const FreeVariable& var) {
        return "FreeVariable("s + var.name() + ")"s;
    }
};
} //namespace

std::string to_string(const LambdaTerm &term) {
    return visit(ToStringVisitor(term), term);
}

std::string representation(const LambdaTerm &term) {
    return visit(RepresentationVisitor{}, term);
}

bool FreeVariable::operator==(const FreeVariable &other) const {
    return name() == other.name();
}

bool FreeVariable::operator!=(const FreeVariable &other) const {
    return !(*this == other);
}

bool equivalent(const LambdaTerm& left, const LambdaTerm& right) {
    return std::visit(overloaded {
        [&right](const Application& app) {
            auto other = maybe_get<Application>(right);
            if (!other)
                return false;
            else
                return equivalent(app.function(), other.value().function())
                        && equivalent(app.argument(), other.value().argument());
        },
        [&right](const Abstraction& abs) {
            auto other = maybe_get<Abstraction>(right);
            if (!other)
                return false;
            else {
                return equivalent(abs.term(), other.value().term());
            }
        },
        [&right](const Variable& var) {
            auto other = maybe_get<Variable>(right);
            if (!other) return false;
            else return var.rank() == other.value().rank();
        },
        [&right](const FreeVariable& fv) {
            auto other = maybe_get<FreeVariable>(right);
            if (!other) return false;
            else return fv.name() == other.value().name();
        }
    }, left);
}

} // namespace lambda
