#include "lambdaparser.hpp"

#include <istream>
#include <locale>
#include <codecvt>

#include <cassert>

#include <map>
#include <set>
#include <vector>

#include <sstream>

namespace lambda::parser {


ParseException::ParseException() : ParseException("", 0, 0) { }

ParseException::ParseException(const std::string& error, size_t line, size_t pos)
    : m_line(line),
      m_pos(pos)
{
    std::ostringstream oss;
    oss << "Parse error at " << m_line << ':' << m_pos << ": " << error;
    m_msg = oss.str();
}

namespace {

std::string toUtf8(const std::wstring& in) {
    std::wstring_convert<std::codecvt_utf8<wchar_t>> convert;
    return convert.to_bytes(in);
}

using std::make_unique;

/**The parser creates an abstract syntax tree for lambda expressions.
 * It uses the following LL(1) Grammar:
 * ```
 * T → A T'
 * T' → T | €
 * A → ID | (T) | L
 * L → \ ID . T
 * ```
 */
class Parser {
public:
    using ptr = std::unique_ptr<LambdaTerm>;

    Parser(std::wistream& source);

    LambdaTerm parseTerm(std::optional<LambdaTerm>&& func = {});
    LambdaTerm parseApplication(LambdaTerm&& func);
    LambdaTerm parseApplicand();
    LambdaTerm parseAbstraction();
    std::wstring parseId();
private:
    size_t m_pos;
    size_t m_line;
    size_t m_index;
    std::wistream& m_source;

    std::map<std::wstring, std::vector<size_t>> m_vars;
    size_t m_rank;

    wchar_t current() { m_c = m_source.peek(); return m_source.peek(); };
    void next();
    wchar_t m_c;

    bool isIdChar(wchar_t c) const;
    bool isLambda(wchar_t c) const;

    size_t eatWhitespace();
    void eat(wchar_t c);


    LambdaTerm lookupName(const std::wstring& name);
    size_t bindName(const std::wstring& name);
    void unbindName(const std::wstring& name);

    LambdaTerm error(const std::wstring& msg) const;
};

Parser::Parser(std::wistream &source)
    : m_pos(0),
      m_line(0),
      m_index(0),
      m_source(source),
      m_rank(0)
{ }

LambdaTerm Parser::parseTerm(std::optional<LambdaTerm>&& f) {
    auto a = parseApplicand();
    if (f)
        return parseApplication(Application(std::move(f.value()), std::move(a)));
    else
        return parseApplication(std::move(a));
}

LambdaTerm Parser::parseApplication(LambdaTerm&& f) {
    eatWhitespace();
    if (m_source.eof() || current() == L')') {
        return std::move(f);
    } else {
        return parseTerm(std::move(f));
    }
}

LambdaTerm Parser::parseApplicand() {
    eatWhitespace();
    if (isIdChar(current())) {
        auto name = parseId();
        return lookupName(name);
    } else if (current() == L'(') {
        eat(L'(');
        auto term = parseTerm();
        eatWhitespace();
        eat(L')');
        return term;
    } else if (isLambda(current())) {
        return parseAbstraction();
    } else if (m_source.eof()){
        return error(L"unexpected end of file");
    } else {
        return error(L"unexpected character '" + std::wstring(1, current()) + L"'");
    }
}

LambdaTerm Parser::parseAbstraction() {
    if (isLambda(current())) {
        next();
        eatWhitespace();
        ++m_rank;
        auto name = parseId();
        eatWhitespace();
        eat(L'.');
        bindName(name);
        auto term = parseTerm();
        unbindName(name);
        --m_rank;
        return Abstraction(toUtf8(name), std::move(term));
    } else if (m_source.eof()){
        return error(L"unexpected end of file");
    } else {
        return error(L"expected lambda but got " + std::wstring(1, current()));
    }
}

std::wstring Parser::parseId() {
    std::wstring name;
    while (isIdChar(current())) {
        name += current();
        next();
    }
    if (name.empty()) {
        error(L"name cannot be empty");
        return L"error";
    } else {
        return name;
    }
}

void Parser::next() {
    if (m_source && current() == L'\n') {
        ++m_line;
        m_pos = 0;
    } else {
        ++m_pos;
    }
    ++m_index;
    m_source.get();
    m_c = m_source.peek();
}

bool Parser::isIdChar(wchar_t c) const {
    return std::isgraph(c) && !(std::ispunct(c) || isLambda(c) || c == '(' || c == ')');
}

bool Parser::isLambda(wchar_t c) const {
    return c == L'\\' || c == L'λ';
}

size_t Parser::eatWhitespace() {
    size_t count = 0;
    while (std::isblank(current())) {
        next();
        ++count;
    }
    return count;
}

void Parser::eat(wchar_t c) {
    if (m_source.eof())
        error(L"unexpected end of file");
    else if (current() != c)
        error(L"expected " + std::wstring(1,c) + L", got " + std::wstring(1, current()));
    else
        next();
}

LambdaTerm Parser::lookupName(const std::wstring &name) {
    auto it = m_vars.find(name);
    if (it == m_vars.end() || it->second.empty()) {
        return FreeVariable(toUtf8(name));
    } else {
        return m_rank - it->second.back();
    }
}

size_t Parser::bindName(const std::wstring &name) {
    return m_vars[name].emplace_back(m_rank);
}

void Parser::unbindName(const std::wstring &name) {
    auto it = m_vars.find(name);
    it->second.pop_back();
    if (it->second.empty())
        m_vars.erase(it);
}

LambdaTerm Parser::error(const std::wstring &msg) const {
    throw ParseException(toUtf8(msg), m_line, m_pos);
}
} // namespace

LambdaTerm parseTerm(std::istream &in) {
    std::wbuffer_convert<std::codecvt_utf8<wchar_t>> conv(in.rdbuf());
    std::wistream win(&conv);
    Parser p(win);
    return p.parseTerm();
}

LambdaTerm parseTerm(const std::string &in) {
    std::stringstream ss(in);
    return parseTerm(ss);
}

} // namespace lambda::parser
