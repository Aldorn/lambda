project(liblambda_test)

# set(TEST_LOGGING unit_scope)
enable_testing()
set(TEST_LOGGING all)

add_compile_definitions(BOOST_TEST_DYN_LINK)

find_package(Boost COMPONENTS unit_test_framework REQUIRED)

add_executable(LibraryTest "LibraryTest.cpp")
target_link_libraries(LibraryTest PUBLIC LibLambda Boost::unit_test_framework)
add_test(NAME "Library Test" COMMAND $<TARGET_FILE:LibraryTest> -l ${TEST_LOGGING})

add_executable(EvaluationTest "EvaluationTest.cpp")
target_link_libraries(EvaluationTest PUBLIC LibLambda Boost::unit_test_framework)
add_test(NAME "Evaluation Test" COMMAND $<TARGET_FILE:EvaluationTest> -l ${TEST_LOGGING})
