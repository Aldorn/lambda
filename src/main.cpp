#include <iostream>
#include <cwctype>
#include <string>
#include <fstream>

#include <sstream>

#include <locale>
#include <codecvt>

#include "lambdaparser.hpp"
#include "evaluators.hpp"

using namespace std;

void showLocale() {
    cout << "LC_ALL=" << setlocale(LC_ALL, nullptr) << endl;
    cout << "LC_CTYPE=" << setlocale(LC_CTYPE, nullptr) << endl;
}

const std::string lmb(R"((\x.\y. x y) (a b (c d)))"s);

void utf8() {
    std::ifstream f("foofile.txt");
    if (!f)
        std::cerr << "konnte Datei nicht öffnen" << std::endl;
    std::wbuffer_convert<std::codecvt_utf8<wchar_t>> conv(f.rdbuf());
    std::wistream in(&conv);
    for (wchar_t c; in.get(c); ) {
        std::cout << c << ' ';
    }
}

std::unique_ptr<int> down(std::unique_ptr<int>&& p) {
    // return p; // error. Why?
    return std::move(p);
}

void up() {
    auto p = std::make_unique<int>(5);
    auto q = down(std::make_unique<int>(7));
}

int main()
{
    cout << "Hello World!" << endl;
    std::setlocale(LC_ALL, "");
    showLocale();

    std::cout << std::boolalpha << std::isalpha(L'\\', std::locale()) << std::endl;

    std::cout << "---------------" << std::endl;
    if (false) {
        auto s = R"(x (\x.x (y x) z) y)"s;
        std::cout << std::boolalpha << s << std::endl;
        std::istringstream iss(s);
        auto term = lambda::parser::parseTerm(iss);

        std::cout << lambda::to_string(term) << std::endl;
        std::cout << lambda::representation(term) << std::endl;
    }
    std::cout << "---------------" << std::endl;
    if (false) {
        std::cout << std::boolalpha << lmb << std::endl;
        std::istringstream iss(lmb);
        auto term = lambda::parser::parseTerm(iss);

        std::cout << lambda::to_string(term) << std::endl;
        std::cout << lambda::representation(term) << std::endl;
        auto& t = std::get<lambda::Application>(term);
        lambda::LambdaTerm t2 = t;
        t.argument() = lambda::Variable(0);
        std::cout << lambda::to_string(term) << std::endl;
        std::cout << lambda::representation(term) << std::endl;
        std::cout << lambda::to_string(t2) << std::endl;
        std::cout << lambda::representation(t2) << std::endl;
    }
    std::cout << "---------------" << std::endl;
    if (true) {
        std::string code(R"((\x.\y. x y) (a b (c d)) (x) (d))"s);
        //std::string code(R"(a b c d)"s);

        std::cout << std::boolalpha << code << std::endl;
        std::istringstream iss(code);
        auto term = lambda::parser::parseTerm(iss);
        std::cout << lambda::to_string(term) << std::endl;

        auto redex = lambda::callByNameRedex(term);
        std::cout << lambda::to_string(redex.value()) << std::endl;

        lambda::reduceRedex(redex.value().get());
        std::cout << lambda::to_string(term) << std::endl;
    }
    std::cout << "---------------" << std::endl;
    if (true) {
        std::string code;
        std::getline(std::cin, code);
        std::istringstream iss(code);
        auto term = lambda::parser::parseTerm(iss);
        //std::cout << lambda::to_string(term) << std::endl;
        auto redex = lambda::callByNameRedex(term);
        bool autoRun = false;
        while (redex && std::cin) {
            std::cout << "term:  " << lambda::to_string(term) << std::endl;
            std::cout << "redex: " << lambda::to_string(redex.value()) << std::endl;
            lambda::reduceRedex(redex.value());
            redex = lambda::callByNameRedex(term);
            std::string tmp;
            if (!autoRun) std::getline(std::cin, tmp);
            if (tmp == "r") autoRun = true;
            // (\n.(\p.p \a.\b.a) (n (\p.(\f.f (p \a.\b.b) (\s.\z. (p \a.\b.a) s ((p \a.\b.b) s z) ))) (\f.f (\s.\z.z) (\s.\z.s z)) ))(\s.\z.s (s z))
        }
        std::cout << "term:  " << lambda::to_string(term) << std::endl;
    }
    std::cout << "---------------" << std::endl;

    return 0;
}
// ,
