#ifndef LAMBDATERM_HPP
#define LAMBDATERM_HPP
#include <memory>
#include <string>
#include <variant>

namespace lambda {

class Variable;
class FreeVariable;
class Application;
class Abstraction;

using LambdaTerm = std::variant<Variable, FreeVariable, Application, Abstraction>;

class Variable {
    size_t m_rank;
public:
    Variable(size_t depth);
    virtual ~Variable() = default;

    bool operator==(const Variable& other) const;
    bool operator!=(const Variable& other) const;

    inline const size_t& rank() const { return m_rank; }
    inline size_t& rank() { return m_rank; }
};

class FreeVariable {
    std::string m_name;
public:
    FreeVariable(const std::string& name)
        : m_name(name)
    { }
    virtual ~FreeVariable() = default;

    bool operator==(const FreeVariable& other) const;
    bool operator!=(const FreeVariable& other) const;

    inline const auto& name() const { return m_name; }
    inline auto& name() { return m_name; }
};

class Application {
    std::unique_ptr<LambdaTerm> m_function;
    std::unique_ptr<LambdaTerm> m_argument;
public:
    Application() = default;
    //explicit Application(std::unique_ptr<LambdaTerm>&& function, std::unique_ptr<LambdaTerm>&& argument);
    Application(LambdaTerm&& function, LambdaTerm&& argument);
    Application(const LambdaTerm& function, const LambdaTerm& argument);
    Application(const Application&);
    Application(Application&&) = default;
    virtual ~Application() = default;
    Application& operator=(const Application& other);
    Application& operator=(Application&& other);

    bool operator==(const Application& other) const;
    bool operator!=(const Application& other) const;

    const LambdaTerm& function() const;
    LambdaTerm& function();

    const LambdaTerm& argument() const;
    LambdaTerm& argument();
};

class Abstraction {
    std::string m_name;
    std::unique_ptr<LambdaTerm> m_term;
public:
    //explicit Abstraction(const std::string& name, std::unique_ptr<LambdaTerm>&& term);
    Abstraction(const std::string& name, LambdaTerm&& term);
    Abstraction(const std::string& name, const LambdaTerm& term);
    Abstraction(const Abstraction&);
    Abstraction(Abstraction&&) = default;
    virtual ~Abstraction() = default;
    Abstraction& operator=(const Abstraction& other);
    Abstraction& operator=(Abstraction&& other);

    bool operator==(const Abstraction& other) const;
    bool operator!=(const Abstraction& other) const;

    const LambdaTerm& term() const;
    LambdaTerm& term();

    const std::string& name() const;;
    std::string& name();
};

using std::visit;

std::string to_string(const LambdaTerm& term);

std::string representation(const LambdaTerm& term);

bool equivalent(const LambdaTerm& left, const LambdaTerm& right);

}

#endif // LAMBDATERM_HPP
